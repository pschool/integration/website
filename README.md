<h1 align="center">Welcome to integrationWebSite 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000&style=for-the-badge" />
</p>

![MIT Licence](https://img.shields.io/github/license/PaulTedesco/Games-Coding-School?style=for-the-badge)
![PHP VERSION](https://img.shields.io/badge/PHP%20Version-8.0-success?style=for-the-badge&logo=php)


> A school project for learn code

### 🏠 [Homepage](https://integration.paul-tedesco.com)

### ✨ [Demo](https://integration.paul-tedesco.com)

## Install

```sh
composer install
```

## User Test

```sh
username: admin
password: admin
```

```sh
username: user
password: user
```


## Author

👤 **Paul Tedesco**

* Website: paul-tedesco.com
* Github: [@PaulTedesco](https://github.com/PaulTedesco)

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [Paul Tedesc](https://github.com/PaulTedesco).

This project is [MIT](./LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_