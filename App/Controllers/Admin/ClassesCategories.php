<?php


namespace App\Controllers\Admin;


use App\Utils\Common;
use App\Utils\Utils;
use JetBrains\PhpStorm\Pure;
use App\Models\ClassesCategories as Classe_categories_model;

class ClassesCategories extends Common
{
    #[Pure] public function __construct()
    {
        parent::__construct("Categories Classes");
    }

    public function show()
    {
        $categories = new Classe_categories_model();
        $allCat = $categories->getAll($categories->classes_categories_schema);
        parent::getView("Admin/classes_categories.twig", params: ["categories" => $allCat], sidebar: true);
    }

    public function create(array $form)
    {
        $categories = new Classe_categories_model();
        $categories->classes_categories_schema->setName($form['name']);
        $categories->classes_categories_schema->setDescription($form['description']);
        $categories->create($categories->classes_categories_schema);
        Utils::goBack();
    }

    public function update($uuid, array $form)
    {
        $categories = new Classe_categories_model();
        $categories->classes_categories_schema->setName($form['name']);
        $categories->classes_categories_schema->setDescription($form['description']);
        $categories->classes_categories_schema->setId($uuid);
        $categories->update($categories->classes_categories_schema);
        Utils::redirect("/admin/classes_cat");
    }

    public function delete($uuid)
    {
        $categories = new Classe_categories_model();
        $categories->classes_categories_schema->setId($uuid);
        $categories->delete($categories->classes_categories_schema);
        Utils::redirect("/admin/classes_cat");
    }
}