<?php


namespace App\Controllers\Admin;


use App\Utils\Common;
use App\Utils\Utils;
use JetBrains\PhpStorm\Pure;
use App\Models\Classes as Classes_model;
use App\Models\ClassesCategories as Classes_categories_model;

class Classes extends Common
{
    #[Pure] public function __construct()
    {
        parent::__construct("Classes");
    }

    public function show()
    {
        $categories = new Classes_categories_model();
        $classes = new Classes_model();
        $allCat = $categories->getAll($categories->classes_categories_schema);
        $allClasses = $classes->getAll($classes->classes_schema);
        parent::getView("Admin/classes.twig", params: ["categories" => $allCat, "classes" => $allClasses], sidebar: true);
    }

    public function create(array $form)
    {
        $categories = new Classes_model();
        $categories->classes_schema->setTitle($form['title']);
        $categories->classes_schema->setContent($form['content']);
        $categories->classes_schema->setClassesCat($form['classes_cat']);
        $categories->create($categories->classes_schema);
        Utils::goBack();
    }

    public function update($uuid, array $form)
    {
        $categories = new Classes_model();
        $categories->classes_schema->setTitle($form['title']);
        $categories->classes_schema->setContent($form['content']);
        $categories->classes_schema->setClassesCat($form['classes_cat']);
        $categories->classes_schema->setId($uuid);
        $categories->update($categories->classes_schema);
        Utils::redirect("/admin/classes");
    }

    public function delete($uuid)
    {
        $categories = new Classes_model();
        $categories->classes_schema->setId($uuid);
        $categories->delete($categories->classes_schema);
        Utils::redirect("/admin/classes");
    }

    public function showCreate()
    {
        $categories = new Classes_categories_model();
        $allCat = $categories->getAll($categories->classes_categories_schema);
        parent::getView("Admin/classes_create.twig", params: ["categories" => $allCat], sidebar: true);
    }
}