/*
 * MIT License
 *
 * Copyright (c) 2021. Paul Tedesco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

create table classes
(
    id          char(36)     not null,
    title       varchar(255) not null,
    content     longtext null,
    classes_cat char(36)     not null,
    constraint classes_id_uindex
        unique (id)
);

alter table classes
    add primary key (id);

create table classes_categories
(
    id          char(36)     not null
        primary key,
    name        varchar(255) not null,
    description longtext     not null
);

create table levels
(
    id          char(36)     not null
        primary key,
    name        varchar(255) not null,
    description longtext     not null,
    response    longtext     not null,
    cat_id      char(36)     not null
);
create table levels_categories
(
    id          char(36)     not null
        primary key,
    name        varchar(255) not null,
    description longtext     not null,
    level       longtext null
);

create table levels_program
(
    id_users   char(36) not null,
    id_levels  char(36) not null,
    start_date varchar(255) null,
    end_date   varchar(255) null
);

create table users
(
    id       char(36)                      not null
        primary key,
    username varchar(255)                  not null,
    email    varchar(255)                  not null,
    password varchar(255)                  not null,
    level    int                           not null,
    role     varchar(255) default 'member' not null,
    lang     varchar(5)   default 'en'     not null,
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username)
);